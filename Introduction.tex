% ===================================================================
%
% INTRODUCTION
%
% AHS Conference Paper 2015 
%
% Authors: Bharath Govindarajan
%          Yong Su Jung
%          James Baeder
%          Jayanarayanan Sitaraman
%
% ===================================================================
\section{Introduction}

The field of computational fluid dynamics has been rapidly evolving over the past several decades, both in terms of the capabilities of meshing geometries as well as the accuracy and efficiency of flow solvers. 
%Accurate modeling of the flow field around rotorcraft components is required to predict blade airloads, fuselage loads and the effects of interactional aerodynamics. 
Aerospace applications in general and more specifically rotorcraft simulations are driven in part by the field of aerodynamics, which has provided major impetus for advances in computational technology. Advances in algorithmic efficiency combined with the leaps in available computational power have enabled numerical modeling tools to substantially reduce wind-tunnel testing times and expedite design processes required for aerospace/rotorcraft systems. Despite these advances, realistic geometries such as the hub or airframe are becoming increasingly complex and improved computational techniques are required to deliver more accurate solutions at lower computational costs. Relatively lower-order numerical techniques used for aerodynamic studies, such as a free-vortex method, vortex particle methods, or panel methods make modeling assumptions that limit their solutions from capturing detailed aerodynamic effects; see Chapter 5 of Ref.~\citenum{Leishman06} for a comprehensive study of such techniques. Therefore, there is a need to use high-resolution numerical formulations to resolve detailed physical phenomenon like vortex/vortex interactions, turbulence and flow separation. It is also important that these solutions are realizable within a practical computational time.

Numerical solutions of partial differential equations, in either two or three dimensions using an Eulerian formulation invariably require the grids to conform to the computational domain. Present day computational fluid dynamics approaches largely utilize either structured or unstructured grid systems for discretizing the computational domain, with the relative benefits between the two methods varying to a wide degree. In general, structured grids provide a well defined structure that allows for clearly defined discretization stencils and pathways for efficient convergence acceleration for implicit schemes using approximate factorization techniques. Furthermore, high-order numerical schemes can be efficiently constructed in structured grids, and there are several well established discretization schemes available in this regard ~\cite{Harten91,Harten97,vanLeer79,Osher84} which are directly linked to the ease of finding neighbouring cells in the computational stencil. 


\begin{figure}
\centering
\subfigure[Structured overset mesh~\cite{Kalra10}]{
   \includegraphics[width=0.48\columnwidth]{../Figures/OversetSystem.pdf}}
\subfigure[Unstructured mesh~\cite{CDAdapco}]{
   \includegraphics[width=0.41\columnwidth]{../Figures/UnstructuredMesh.png}}
\caption{Representative system of structured and unstructured meshes typically used.}
\label{fig:MeshSystem}
\end{figure}


While structured meshes around simple bodies of revolution can be constructed with relative ease, it is often only practical to use a system of such grids in either a multi-block or overset fashion for more complicated geometries. Figure~\ref{fig:MeshSystem}(a) shows an example of an overset mesh system that utilizes vortex tracking grids beneath a hovering rotor operating in ground effect. The primary reason for using multiple grids is to selectively cluster a given physical region with a high cell density to accurately capture the underlying physics, such as accurately capturing the convection of the tip-vortex as it interacts with the ground (Fig.~\ref{fig:MeshSystem}(a)). Structured grids are however difficult to generate for complex geometry and hence suffer from excessive lead-time in grid generation before the simulation can be initiated. 
%Furthermore, complex immersed grid technologies (e.g., chimera meshing~\cite{Kalra10}) are necessary to handle relative motion of bodies, such as a rotor blade, where the blade mesh around the rotor cuts through the background mesh requiring the connectivity information between the cells to be established at each time step. 

Unstructured grids provide an advantage over structured grids where a complex
geometry can be discretized into the elementary grid cells with minimal effort;
see Fig.~\ref{fig:MeshSystem}(b). As a result, many current commercial
simulation software rely on unstructured grids and solution methods. These
methods make use of either a collection of simple elements (triangles in
two-dimensions and tetrahedral/prisms in three-dimensions) or mixed elements
with irregular connectivity to mesh the region of interest,
\textcolor{Blue}{which provides greater flexibility for discretizing complex
domains.  Various unstructured meshing techniques are discussed in detail in
Ref.~\citenum{Mavriplis97}.}

% This formulation not only provides greater flexibility for discretizing complex
% domains, but also enables straightforward implementation of adaptive meshing
% techniques where mesh points may be added, deleted, or moved about with the
% mesh connectivity being updated only locally to enhance solution accuracy.
% Various unstructured meshing techniques are discussed in detail in
% Ref.~\citenum{Mavriplis97}.

Unstructured grids, however, have disadvantages in terms of efficiency and accuracy. In general, structured grid flow solvers are faster than their unstructured counter part in achieving similar level of solution convgence. This is
especially true for unsteady time-dependent calculations, where rapid initial
convergence trends that reduces the residual by couple of orders of magnitude
in few sub-iterations is deemed necessary. Convergence acceleration on
unstructured grids is often achieved using Newton-Krylov solvers and multi-grid
techniques, which especially for unsteady viscous flow problems, are not as
efficient as the line-implicit methods commonly used in structured grid
solvers. References~\citenum{mavriplis:aiaa2014} and
~\citenum{lakshminarayan:aiaa2016} illustrate 
some of these issues for time-dependent calculations. The difficulty in identifying a suitable reconstruction scheme that uses neighbouring cells or even
larger neighbour groups results in most unstructured grid solvers at best being only second-order accurate in space~\cite{Diskin:jcp}. The accuracy issues related to usage of unstructured finite volume solvers are illustrated clearly in the prediction of helicopter rotor wakes, where structured grid based codes 
with high-order type reconstruction scheme show improved prediction of rotor
wake interaction compared to their unstructured
counterparts~\cite{biedron:aiaa2008,jayaraman:ahs2016}. Several methods have
been proposed to remedy this scenario, such as finite-element type
discretization~\cite{Burgess12}, spectral volume methods~\cite{Wang06} and
spectral element methods~\cite{Rosenberg06} to name a few. However, all of
these methods have large penalties in terms of efficiency and robustness and
are still emerging in various research roles.

Efforts have been made by past researchers to find ``linelets", i.e., pseudo lines in an unstructured grid, in an attempt to accelerate unstructured grid algorithms~\cite{Martin92,Hassan89}. While it was shown that approximations could be made to certain line-implicit methods to facilitate solution convergence and reduce memory requirements on a single processor, it was observed that these benefits were not realized when used in conjunction with multiple processors. Most of these solution algorithms showed limited success because of the difficulties in identifying lines in pure unstructured grids and even when they were identified it proved difficult to achieve nesting that is required for approximate factorization methods; see Refs.~\citenum{Venkatakrishnan95,Mavriplis06} for an extended discussion on this aspect. However, some of the ideas in these works have been utilized to construct efficient unstructured grid solvers, by using linelets in the wall-normal direction to relieve stiffness and accelerate convergence~\cite{mavriplis:aiaa2014}.

The idea of strand grids in the wall-normal direction has been explored in previous studies~\cite{Meakin07,Wissink09,Katz11} for improving the automation, efficiency and accuracy of complex geometry simulations. One of the basic tenets of strand grids is to provide structure in the wall normal direction, such that stencil based discretization and line-implicit methods can be used. While the use of strands grids in the wall-normal direction (i.e., in the prismatic layers) has helped accelerate convergence trends, the other two surface directions are primarily comprised of unstructured meshes and limit the gains achievable by the use of strand grids. Recently, Hamiltonian paths have been used on a purely unstructured two-dimensional triangular mesh to obtain lines along which line-implicit methods can be used~\cite{Sitaraman14}. The primary idea lies in the subdivision of triangles into multiple quadrilaterals and connecting cell-faces so as to form a unique set of paths that serve as the surface linelets. The methodology developed in Ref~\cite{Sitaraman14} showed very promising results for spatial accuracy and convergence trends. However, the application was limited only to two spatial dimensions and execution on a serial processor. To be competitive for real applications, this methodology needs to be enhanced further for application in three dimensional flows and execution on massively parallel compute systems. The work performed in this paper is directly responsive to this requirement.

The objective of this work is to explore structure in unstructured grids to leverage the benefits of stencil-based discretization and approximate factorization in an unstructured grid framework. To this end, the present work uses the idea of Hamiltonian paths (in the surface directions) and strand grids (in the wall-normal direction) generated from a purely unstructured grid to obtain high-order solutions by exploiting approximate factorization techniques and reconstruction schemes. The method of constructing the Hamiltonian paths, strand grids, and their decomposition across many subdomains (for parallel execution) is also discussed. Representative two-dimensional and three-dimensional solutions, such as flow over an airfoil, wing, sphere, and fuselage are studied. The obtained results are compared against an established structured CFD solver, with emphasis being placed on the accuracy and convergence trends, with the longer-term goal being the accurate modeling of the flow field around rotorcraft components in a cost efficient manner.

% ===================================================================
% END OF FILE
% ===================================================================
