% ===================================================================
%
% METHODOLOGY
%
% AHS Conference Paper 2015 
%
% Authors: Bharath Govindarajan
%          Yong Su Jung
%          James Baeder
%          Jayanarayanan Sitaraman
%
% ===================================================================
\section{Methodology}
% ===================================================================

% ===================================================================
\subsection{Quadrilateral Subdivision and Hamiltonian Paths}
% ===================================================================
\begin{figure*}
\centering
\includegraphics[width=\textwidth]{../Figures/HamiltonianPathSchematic.pdf}
\vspace{-0.3in}
\caption{Schematic showing the quadrilateral subdivision of a triangle and the construction of Hamiltonian paths.}
\label{fig:HamiltonianPathSchematic}
\end{figure*}

The goal of the subdivision process is to be able to extract lines in a purely
unstructured grid so that the advantages of line-based methods can be
exploited. This subdivision is explained using an unstructured triangular grid
around a NACA 0012 airfoil, as shown in
Fig.~\ref{fig:HamiltonianPathSchematic}. Every triangle in the unstructured
grid is divided into three \textcolor{Blue}{quadrilateral} elements and
each of these quadrilaterals can be refined into further multiple
quadrilaterals (Fig.~\ref{fig:HamiltonianPathSchematic}(a)). Distinct loops
are formed by connecting the midpoint of the faces of the quadrilateral cells
of all the triangles surrounding a particular triangular node.
Figure~\ref{fig:HamiltonianPathSchematic}(b) shows the initial unstructured
triangular mesh around the NACA 0012 airfoil.
Figure~\ref{fig:HamiltonianPathSchematic}(c) shows the original triangular
mesh with the quadrilateral subdivisions (twelve quadrilateral for each
original triangular cell) along with the Hamiltonian paths that pass through
the centroid of each quadrilateral. Unique Hamiltonian paths can be generated
around each vertex of the original triangular mesh
(Fig.~\ref{fig:HamiltonianPathSchematic}(b)). Notice that the intersecting
Hamiltonian paths are of different color (obtained using a greedy colouring
algorithm~\cite{Irani94}), which in turn promotes nesting of the paths and
hence facilitates stencil based discretization and approximate factorization
of the implicit operator. The formed Hamiltonian loops are either closed
(those of interior cells) or are open and end at either the solid boundary or
at the far field. Similar to a structured grid, the Hamiltonian loops can be
thought of as lines along which line-implicit techniques can be implemented.

\begin{figure}
\centering
\subfigure[Multiple level quadrilateral subdivision]{
\includegraphics[width=0.45\columnwidth]{../Figures/MultiQuadDiv_a.png}}
\subfigure[Airfoil mesh obtained with quad-level 1 and quad-level 2 subdivision]{
\includegraphics[width=0.45\columnwidth]{../Figures/MultiQuadDiv_b.png}}
%\vspace{-0.1in}
\caption{Multiple quadrilateral subdivision using: (a) representative triangle, and (b) NACA 0012 airfoil.}
\label{fig:MultiQuadDiv}
\end{figure}

\begin{figure}
\centering
\subfigure[Multiple quad-level subdivision of an isosceles
triangles (all meshes have equal number of quad cells)]{
\includegraphics[width=0.45\columnwidth]{../Figures/MeshQuality_a.png}}
\subfigure[Equiangle skewness for multiple quad-level meshes]{
\includegraphics[width=0.45\columnwidth]{../Figures/MeshQuality_b.pdf}}
%\vspace{-0.1in}
\caption{Multiple quadrilateral subdivision for an isosceles triangle and associated equiangle skewness.}
\label{fig:MeshQuality}
\end{figure}

A given triangle is subdivided into three quadrilaterals and these
quadrilaterals can be subdivided an arbitrary number of times (henceforth
referred to as different levels of quadrilateral subdivision, or quad-level).
After one additional level of quadrilateral subdivision, the resulting meshes
have four times the number of quadrilateral cells with twice the number of
Hamiltonian loops; each loop now being twice as long where the loops are
denoted by red, green, or blue colours and the mesh is represented by the gray
lines (see Figure~\ref{fig:MultiQuadDiv}(a)). Different quad-levels also serve
as efficient ways to perform grid-refinement and grid-convergence studies.
Figure~\ref{fig:MultiQuadDiv}(b) shows the NACA 0012 airfoil grid subdivided to
quad-level 1 and quad-level 2. Note that in this example, the newly created
points on the edges of the triangles are flushed to the airfoil surface using
the NACA analytical equations for the surface definition~\cite{Ladson96}.
Furthermore, the subdivision capability has potential applicability to a
multigrid methodology (not explored in the present study), where the solution
can be solved initially on a coarser grid (small quad-level) and then moved to
a finer grid (larger quad-level) to remove the high-frequency error content and
accelerate convergence. 

The additional subdivision of quadrilaterals can also be used to potentially
improve the quality of the mesh cells and that of the Hamiltonian loops.
Figure~\ref{fig:MeshQuality}(a) shows three meshes formed using triangular
grids: (a) Mesh 1 is formed with a triangular grid consisting of $n$ points,
with quad-level 1, (b) Mesh 2 contains $n/4$ points (alternate points removed),
with quadrilateral subdivision to quad-level 2, and (c) Mesh 3 contains $n/16$
points with subdivision to quad-level 3. Every additional quad-level increases
the number of cells by four, and therefore, by way of construction the three
meshes have the same total number of quadrilateral cells. It can be seen from
Fig.~\ref{fig:MeshQuality}(a) that the Hamiltonian loops formed in Mesh 3 are
longer and smoother (larger radius of curvature) compared to the loops seen in
Mesh 1 or Mesh 2. The longer Hamiltonian loops can potentially help in
convergence because the corresponding line-operators will be longer and fewer
in number.

The quality of the meshes obtained at the various quad-levels are compared using an equiangle skewness indicator~\cite{Wang06b}, given by
\begin{equation}
\textrm{Equiangle skewness} = \textrm{max} \left[ \dfrac{\theta_{\rm max} - \theta_e}{180-\theta_e} , \frac{\theta_e -\theta_{\rm min}}{\theta_e} \right]
\label{eqn:skewness}
\end{equation}
\noindent where $\theta_{\rm max}$ and $\theta_{\rm min}$ are the largest and
smallest internal angles in the cell, respectively, and $\theta_e$ is the
equiangular value for the cell (90$^\circ$ for quadrilateral). Values of
equiangle skewness range between 0 and 1, with a value of 0 representing cells
with all angles equiangular, and a value of 1 representing a highly skewed
cell, respectively. Therefore, cells with skewness values closer to 0 are
deemed cells of high quality. Figure~\ref{fig:MeshQuality}(b) shows a histogram
of the cell quality computed using equiangle skewness for three different
quad-levels formed from the mesh of isosceles triangles; see
Fig.~\ref{fig:MeshQuality}(a). As the quad-level increases, the skewness value
transitions from predominantly spikes (red) to a smoother distribution
(yellow), as shown in Fig.~\ref{fig:MeshQuality}(b). This increased cell
quality is a consequence of fewer quadrilateral cells at the corners of the
original triangle mesh, which results in skewed cells. The overall quality of
Mesh 3 is greater than Mesh 2, which in turn is greater \textcolor{Blue}{than} the quality of
Mesh 1. Therefore, an initial coarser mesh with a larger number of
quadrilateral subdivisions results in a mesh with higher cell quality compared
to a finer initial mesh with a smaller number of quadrilateral subdivisions.

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{../Figures/CellSmoothing.png}
\caption{Schematic depicting the addition of internal nodes within a triangle and the subsequent smoothing of these nodes.}
\label{fig:CellSmoothing}
\end{figure}

The process of subdivision requires the insertion of points within a given
triangle to form quadrilateral cells, and subsequently, the Hamiltonian loops.
Ideal loops are those of high-radius of curvature and ones with the segment
lengths \textcolor{Blue}{change} gradually from one cell to the next along a given loop.
Therefore, it is necessary to perform a smoothing of the nodes to ensure
high-order accuracy and convergence of the solution. A naive implementation
involves the placement of the internal nodes in any arrangement followed by a
smoothing operation, such as a Laplacian smoothing, to ensure smooth
Hamiltonian paths. In this scenario, any given node that is not on the domain
boundaries is repositioned based on the average of the position of the
connecting nodes and the process is repeated till the positions do not change
between subsequent iterations. However, a drawback with Laplacian smoothing is
that the cell sizes within a given triangle show wide variations, with the
largest cells present at the corners of the triangle and the smallest cell at
the interior, as shown in Fig.~\ref{fig:CellSmoothing}(a).

Strategies were investigated to obtain meshes with cells of equal area through
careful placement of the interior nodes within the triangles, as shown in
Fig.~\ref{fig:CellSmoothing}(b). Loops with the least change in curvature were
obtained when the nodes within each subdivided quadrilateral were placed along
straight lines. However, this distribution of the nodes resulted in the cell
sizes varying within the triangle. Another distribution of nodes such that the
resulting quadrilaterals are all of equal area, forming a shape-preserving
pattern is shown in Fig.~\ref{fig:CellSmoothing}(b). The loops formed with
this distribution had varying curvature. Therefore, a blended mesh was
obtained that averaged the node positions of the previous two node
distributions, i.e., that of along straight lines and equal area. 
\textcolor{Blue}{The blended mesh results in a node distribution that
results in a balance between equal cell area and smooth loops.}
%%%
%%% Bharath, can you rethink this sentence, I am not quite sure what
%%% this means. I tried to fix it
%%%

% \textcolor{Blue}{\bf The blended mesh which averages the node positions along
% straight lines and maintains equal area, produced a distribution that was more
% appealing that either procedure by themselves.}

The generation of Hamiltonian loops was also extended to hybrid meshes
\textcolor{Blue}{comprised} of both quadrilateral and triangular elements.
Figure~\ref{fig:HamiltonianLoops} shows a schematic of a hybrid mesh system
where structured quadrilateral cells transition to an unstructured triangular
mesh system. These triangles are sub- divided to create quadrilaterals and the
Hamiltonian paths are generated using the same algorithm described before. For
the triangular nodes on the edge of the structured mesh, the Hamiltonian loops
formed traverse through the quadrilateral cells in the unstructured domain and
continue through the structured domain forming curvilinear lines. Coloring is
performed using the aforementioned Greedy coloring algorithm such that proper
nesting of the paths can be achieved to support line-implicit inversion
algorithms. Therefore, there is no additional modification required in the
flow solver as the hybrid mesh has now been effectively defined as a
collection of Hamiltonian loops, both open and closed.

\begin{figure}
\centering
\includegraphics[width=0.8\columnwidth]{../Figures/HamiltonianLoops.png}
\caption{Schematic depicting the creation of Hamiltonina loops on: (a) All triangular mesh, and (b) Hybrid triangular/quadrilateral mesh.}
\label{fig:HamiltonianLoops}
\end{figure}

\textcolor{ForestGreen}{A few comments can be made on the creation of Hamiltonian loops and the subsequent addition of points within the cells
\begin{enumerate}
\item The process of finding loops in a subdivided quadrilateral mesh is unique and robust, i.e., for a given mesh there is only one configuration of Hamiltonian loops that exist. This observation stems from the fact that a face (or an edge in two-dimensions) permits only a single predetermined direction of the loop passing through it. Therefore, by extension to the entire mesh, there is guaranteed to exist a unique configuration of the Hamiltonian loops.
\item The addition of increasing the quad-level will always produce a mesh that does not create negative volume cells. The points are added based on a simple iso-parametric mapping of a quadrilateral. Therefore, it is always possible to create these quadrilaterals using a straight line subdivision in the computational space, which is then mapped to the physical space. The process of smoothing is done through a standard Laplacian operator, which ensures that these lines never cross for any arbitrary quadrilateral subdivision. 
\end{enumerate}}

% The resulting blended mesh produced a distribution that is more appealing
% than either procedure by themselves and the quadrilateral cell sizes are more
% uniform than with the previous Laplacian smoothing; see
% Fig.~\ref{fig:CellSmoothing}(a).

% ===================================================================
\subsection{Strand Grids}
% ===================================================================
To extend the formulation to three-dimensions, strand based grids have been
employed in the present work, which provides a structure in the wall normal
direction. Consequently, these strand grids allow for line-implicit methods to
be used along the strand direction. Layers of hexahedra are generated by
extruding the surface in the wall normal direction to create strands.
\textcolor{Blue}{The advantages of this technique over traditional
unstructured methods are: 1. Linelets are clearly identified in all spatial
directions enabling high-order type reconstruction schemes, and 2. The connectivity of the
Hamiltonian paths are preserved across the multiple layers. The methodology is
also readily amenable to line-based parallelization techniques.  Therefore,
strand grids allow for accurate discretization techniques and fast implicit
solution schemes usually only available to block structured solvers.  While not
a focus of the present effort, the preservation of loop connectivity across
multiple strand layers can be used to save on computational time and memory.}

% advantages of this technique over traditional unstructured methods are: 1.
% linelets are clearly identified in all spatial directions enabling high-order
% solutions, 2. the connectivity of the Hamiltonian paths are preserved across
% the multiple layers, saving on memory and computational time, and 3. the
% methodology is readily amenable to parallelization techniques. Therefore,
% solver strategies that take advantage of line-based methods are used in the
% present work in the wall-normal strand direction as well.

\begin{figure}
\centering
\includegraphics[width=0.6\columnwidth]{../Figures/StrandSchematic.pdf}
%\vspace{-0.3in}
\caption{Schematic depicting the creation of strand layers from multiple layers of Hamiltonian surface loops.}
\label{fig:StrandSchematic}
\end{figure}

Strand grids emanate from the surface of a body in approximately the
wall-normal direction and provide structure in the third spatial direction.
Therefore, for a given surface geometry, Hamiltonian loops are constructed on
the surface of the body representing two ``in-plane" spatial directions and the
strands represent the third ``out-of-plane" spatial direction.
Figure~\ref{fig:StrandSchematic}(a) shows a schematic of the construction of
the strand grids from a triangular mesh element. This triangular element is
subdivided into quadrilaterals and the construction of Hamiltonian loops around
a given triangular node can be performed by the subdivision of the triangles
associated with the particular node. Therefore, two loops pass through each
quadrilateral and are the local coordinate directions. Strand grids emanate
from the cells on the surface of the body and extrude in the wall-normal
direction. These strand grids pass through multiple layers of Hamiltonian loops
and form the third cell coordinate direction, as shown in
Fig.~\ref{fig:StrandSchematic}(a). Figure~\ref{fig:StrandSchematic}(b) shows
strand grids emanating from a triangular surface cell. Each strand grid is
based on a strand template where the mesh spacing on each strand grid is a
constant and each strand is uniquely defined by the root position and normal
direction. 

% ===================================================================
\subsection{Governing Equations}
% ===================================================================

In the present work, the governing equations used in the mesh-based solver  are the three dimensional, unsteady, compressible Navier--Stokes equations that can be expressed in divergence form as
\begin{equation}
\frac{\partial \textbf{q}}{\partial t} + \nabla\cdot\left[ (\textbf{F}_C-\textbf{F}_V),(\textbf{G}_C-\textbf{G}_V),(\textbf{H}_C-\textbf{H}_V) \right] = 0
\label{eqn:NS}
\end{equation}
\noindent where $\textbf{q}$ is the vector of conserved variables $[ \rho, \rho u, \rho v, \rho w, e ]^T$, $()_C$ and $()_V$ correspond to the convective and viscous fluxes, respectively. Variables $\rho$, $u$, $v$, $w$ and $e$ denote the local flow density, three components of velocity and total energy, respectively. The conservative fluxes are given by
\begin{eqnarray}
\textbf{F}_C(\bf q) &=& [\rho u,\ \rho u^2 +p,\ \rho uv,\ \rho uw,\ (e+p)u ]^T \nonumber \\
\textbf{G}_C(\bf q) &=& [\rho v,\ \rho uv,\ \rho v^2+p,\ \rho vw,\ (e+p)v ]^T \nonumber \\
\textbf{H}_C(\bf q) &=& [\rho w,\ \rho uw,\ \rho vw,\ \rho w^2+p,\ (e+p)w ]^T
\label{eqn:ConsFlux}
\end{eqnarray}
\noindent where $p$ is the pressure and is obtained from ideal gas equation. The viscous fluxes are given by
\begin{eqnarray}
\textbf{F}_V(\bf q) &=& [0,\ \tau_{\rm xx},\ \tau_{\rm yx},\ \tau_{\rm zx},\ u\tau_{\rm xx}+v\tau_{\rm xy}+w\tau_{\rm xz}-\overline{q}_x]^T \nonumber \\
\textbf{G}_V(\bf q) &=& [0,\ \tau_{\rm xy},\ \tau_{\rm yy},\ \tau_{\rm zy},\ u\tau_{\rm yx}+v\tau_{\rm yx}+w\tau_{\rm yz}-\overline{q}_y]^T \nonumber \\
\textbf{H}_V(\bf q) &=& [0,\ \tau_{\rm xz},\ \tau_{\rm yz},\ \tau_{\rm zz},\ u\tau_{\rm zx}+v\tau_{\rm zy}+w\tau_{\rm zz}-\overline{q}_z]^T
\label{eqn:ViscFlux}
\end{eqnarray}
\noindent where $\tau$ is the stress tensor and $q$ are the rates of thermal conduction, which can be represented based on Fourier's law~\cite{Sonntag03}. Implicit discretization using a finite-volume formulation and first-order backward time stepping (field data collocated at the cell centers), yields a discrete form of the governing equation as
\begin{equation}
\frac{\textbf{q}^{n+1} - \textbf{q}^n}{\Delta t} = - \sum_{i\ \in \ \textrm{nfaces}} \hat{\textbf{F}}_i(\textbf{q}^{n+1}) \cdot ds_i
\label{eqn:DiscNS}
\end{equation}
Linearizing the right hand side of the discretized governing equation (Eq.~\ref{eqn:DiscNS}) yields an algebraic system of equations that has to be inverted at each cycle to evolve the solution, i.e.,
\begin{equation}
\left[ \frac{I}{\Delta t} + \frac{\partial \textbf{R}}{\partial \textbf{q}} \right] \Delta \textbf{q} = -\textbf{R}(\textbf{q}^n)
\label{eqn:ImplicitNS}
\end{equation}
\noindent where $\textbf{R}(\textbf{q}^n)$ is the solution residual on the right hand side of Eq.~\ref{eqn:DiscNS} evaluated at time level $n$, i.e.,
\begin{equation}
\textbf{R}(\textbf{q}^n) = - \sum_{i\ \in \ \textrm{nfaces}} \hat{\textbf{F}}_i(\textbf{q}^{\textcolor{Blue}{\bf n}}) \cdot ds_i
\label{eqn:Residual}
\end{equation}
\noindent where $\hat{\textbf{F}}$ is the numerical flux at each face, which is composed of the convective and viscous flux, and $d\textbf{s}_i$ is the area vector normal to that face. For time accurate solutions, both first-order and second-order backward time stepping methods are used along with the dual-time stepping technique to improve the temporal accuracy. A second-order backward time-stepping results in
\begin{equation}
\left[ \frac{I}{\Delta t^*} + \left(\frac{\partial \textbf{R}}{\partial \textbf{Q}} \right)^p \right]\Delta \textbf{Q}^p = -\left[ \frac{3\textbf{Q}^p - 4\textbf{Q}^n + \textbf{Q}^{n-1}}{2\Delta t} + \textbf{R}(\textbf{Q}^p)\right]
\end{equation}
where $p$ indicates the sub-iteration stage in pseudo time ($\tau$) and $\Delta t^*\ =\ (2\Delta t/3)/(1\ +\ 2\Delta t/3\Delta \tau)$.

In a conventional structured solver, spatial reconstruction and implicit
schemes are performed along lines. Similarly, in the Hamiltonian solver, these
schemes are performed along the previously described Hamiltonian loops
\textcolor{Red}{and} strand grids. Note that the present Hamiltonian solver
does not discriminate between Hamiltonian loops and strand grids and
\textcolor{Blue}{processes} all of them equally.

% While this interpretation of the loop does not exploit the template nature of
% strands and save on computational memory, it is beyond the scope of the present
% work.

\begin{figure}
\centering
\subfigure[Flux reconstruction]{
\includegraphics[width=0.32\columnwidth]{../Figures/FaceReconstruction.png}}
\subfigure[Average at node points]{\includegraphics[width=0.32\columnwidth]{../Figures/Crossterm1.png}}
\subfigure[Velocity gradient at faces]{\includegraphics[width=0.32\columnwidth]{../Figures/Crossterm2.png}}
\caption{Neighbourhood of a quadrilateral cell, associated Hamiltonian paths and cell-face variable reconstruction strategy.}
\label{fig:FaceReconstruction}
\end{figure}

Numerical discretization entails evaluation of $\hat{\textbf{F}}$ in
Eq.~\ref{eqn:Residual} such that it satisfies all of the stability, accuracy
and consistency criteria. This numerical flux is evaluated in the present work
using Roe's~\cite{Roe81} approximate Riemann solver which has the general form
\begin{equation}
\hat{\textbf{F}} = \frac{1}{2} \lbrace (\textbf{F}(\textbf{q}_L)+\textbf{F}(\textbf{q}_R)) - | A_{\rm Roe}(\textbf{q}_L,\textbf{q}_R)|\ (\textbf{q}_R - \textbf{q}_L)\rbrace
\label{eqn:RoeFlux}
\end{equation}
\noindent where $\textbf{q}_L$ and $\textbf{q}_R$ are the left and right states
at a given face, respectively. Unlike traditional unstructured grids where the
left and right states are computed using gradients of field variables,
reconstruction is accomplished in the present work using the Hamiltonian loops.
Figure~\ref{fig:FaceReconstruction} shows a schematic of loops passing through
a quadrilateral cell and the associated cell and face numbering schemes, where
the reconstructions are performed along the cells of given loops, similar to a
structured grid. In the present work, the MUSCL scheme with Koren's
differentiable limiter~\cite{Koren93}, the WENO5~\cite{Jiang96} and Compact
Reconstruction WENO (CRWENO)~\cite{Ghosh12} schemes are implemented, which are
third and fifth order accurate in space, respectively. \textcolor{Blue}{It
should be noted that no special treatment to the curvature of the loops was
performed in this study and the weights used in the reconstruction schemes
correspond to their original derivations on equispaced Cartesian grids. }

\textcolor{Red}{The viscous flux were calculated using velocity gradients
obtained from second-order central difference discretizations.
Figures~\ref{fig:FaceReconstruction}(b) and \ref{fig:FaceReconstruction}(c)
show the schematic procedure of the contribution of the cells towards the
stream wise and cross-term evaluation of velocity gradients for the viscous
flux terms. Equation~\ref{eqn:Cross} explains the procedure for calculating the
velocity gradients in two-dimensions at each face between two cells using
finite-differences, where $\phi$ is the $x$-direction velocity $(u)$ or the
$y$-direction velocity $(v)$, and $\xi$, $\eta$ are the spatial directions in
two-dimensions. The node values $\phi_a$, $\phi_b$ are found by averaging the
values at cell centers, as shown in Fig~\ref{fig:FaceReconstruction}(b). The
velocity gradients at the face between two cells (denoted by ‘0’ and ‘1’) are
then computed as shown in Eq.~\ref{eqn:Cross}. This procedure is performed for
all faces along the Hamiltonian loops. For the boundary condition, $\phi$ is
also stored at boundary face centroids, and the boundary diffusion flux can be
linearized in the same manner as at an interior face.}

\begin{equation}
\begin{aligned}
\frac{\partial \phi}{\partial x} = \xi_x\frac{\partial \phi}{\partial \xi}+ \eta_x\frac{\partial \phi}{\partial \eta}\\
\frac{\partial \phi}{\partial y} = \xi_y\frac{\partial \phi}{\partial \xi}+ \eta_y\frac{\partial \phi}{\partial \eta}
\end{aligned}
\label{eqn:Cross}
\end{equation}

\textcolor{red}{\noindent where $\phi_\xi=\phi_1-\phi_0$, $x_\xi=x_1-x_0$, $y_\xi=y_1-y_0$,
$\phi_\eta=\phi_b-\phi_a$, $x_\eta=x_b-x_a$,$y_\eta = y_b-y_a$.}

% %\vspace{-0.3in}
% \begin{figure}%[t!]
% \hspace*{\fill}
% \subfigure[Average at node points]{\includegraphics[width=0.26\textwidth]{../Figures/Crossterm1.png}}\hfill
% \subfigure[Velocity gradient at faces]{\includegraphics[width=0.26\textwidth]{../Figures/Crossterm2.png}}
% \hspace*{\fill}
% \caption{Schematic showing the stream-wise and cross-term evaluation of the viscous flux terms.}
% \label{fig:Crossterm}
% \end{figure}

\textcolor{red}{To consider the effects of turbulence, the Baldwin--Lomax algebraic eddy
viscosity turbulence model~\cite{Baldwin78} and the Spalart--Allmaras one-
equation turbulence model~\cite{Spalart92} is included. The Baldwin--Lomax
model is based on Prandtl's mixing length hypothesis, and the Spalart--
Allmaras (SA) turbulence model employs a single transport equation for the
eddy viscosity, and can be readily implemented on structured grids or on
unstructured grids.}

% ===================================================================
\subsection{Implicit Operator and Approximate Factorization}
% ===================================================================
The implicit operator can be formed by Newton-linearization of the non-linear 
residual (Eqn~\ref{eqn:ImplicitNS}).  As stencil based discretizations are 
used in all three directions, this operator can be further split as:
\begin{equation}
\left[ \frac{I}{\Delta t} + \frac{\partial \textbf{R}}{\partial \textbf{q}} \right] = \left[ \frac{I}{\Delta t} + \frac{\partial R^\zeta}{\partial \textbf{q}} + \frac{\partial R^\eta}{\partial \textbf{q}}+ \frac{\partial R^\xi}{\partial \textbf{q}}\right]
\label{eqn:ImplicitOperator}
\end{equation}
Use of the full implicit operator is intractable in most cases since the 
linear system becomes too large for realistic problems with many thousand 
degrees of freedom. Therefore, the strength of the implicit solution 
process lies in approximating the implicit operator to facilitate
efficient inversion, while ensuring that the 
approximate operator has a similar eigen structure as the 
full operator~\cite{Buelow01}. The usage of Hamiltonian-loops for
discretization provides a pathway 
for achieving this goal, where all three directions of discretization can 
be approximated by constructing a first-order linearization of the right hand side, and therefore, entails contributions only from the neighbouring cells, as shown in Fig.~\ref{fig:FaceReconstruction}. 
\noindent where the operators (with only first-order terms) are given by
\begin{eqnarray}
\frac{\partial R^x}{\partial \textbf{q}} &=& [\textbf{A}_x]\Delta \textbf{q}_{x-1} +  [\textbf{B}_x]\Delta \textbf{q}_{\rm jkl} +  [\textbf{C}_x]\Delta \textbf{q}_{x+1} \nonumber \\
\textbf{A}_x &=& \frac{\partial \hat{\textbf{F}}_{x-1/2}}{\partial \textbf{q}_R} \nonumber \\ 
\textbf{B}_x &=& \frac{\partial \hat{\textbf{F}}_{x-1/2}}{\partial \textbf{q}_L} - \frac{\partial \hat{\textbf{F}}_{x+1/2}}{\partial \textbf{q}_R} \nonumber \\
\textbf{C}_x &=& -\frac{\partial \hat{\textbf{F}}_{x+1/2}}{\partial \textbf{q}_L} 
\label{eqn:Operators}
\end{eqnarray}
where $x$ denotes each of the three spatial directions, $\zeta$, $\eta$ and $\xi$, and their indices $j$, $k$, and $l$, respectively, along the Hamiltonian loops. Terms $\partial \hat{\textbf{F}}/\partial \textbf{q}_L$ and $\partial \hat{\textbf{F}}/\partial \textbf{q}_R$ are the linearizations of the Roe flux function with respect to the left and right states, respectively.

In the present work, three line-based approximate factorization techniques are used, namely the alternating direction implicit (ADI), diagonally dominant ADI (DDADI) and the diagonally dominant line Gauss Seidel (DDLGS). For the ADI scheme, the implicit operator in Eq.~\ref{eqn:ImplicitOperator} is factorized in the coordinate directions given by the Hamiltonian loops and strand grids with the factorized system being expressed as
\begin{equation}
\left[ \frac{I}{\Delta t} + \frac{\partial R^\zeta}{\partial \textbf{q}} \right]\left[ \frac{I}{\Delta t} + \frac{\partial R^\eta}{\partial \textbf{q}} \right]\left[ \frac{I}{\Delta t} + \frac{\partial R^\xi}{\partial \textbf{q}}\right]\Delta \textbf{q} = -\textbf{R}(\textbf{q}^n)
\label{eqn:ADI}
\end{equation}
Each of the above operators have a block tridiagonal (or periodic block tri-diagonal in the case of complete loops) structure, which are inverted directly using a variant of the popular Thomas algorithm~\cite{Conte72}. In the case of DDADI, the factorized system is written such that there is a more diagonally dominant term that aids in solution convergence and adds to the numerical stability of the scheme when compared to the ADI scheme. The DDADI factored scheme is
\begin{equation}
(\textbf{D}+\textbf{O}_\xi) \textbf{D}^{-1} (\textbf{D}+\textbf{O}_\eta) \textbf{D}^{-1} (\textbf{D}+\textbf{O}_\zeta) \Delta \textbf{q} = -\textbf{R}(\textbf{q}^n)
\label{eqn:DDADI}
\end{equation}
\noindent where the diagonal term $\textbf{D}$ and the $\textbf{O}$ matrices are given by
\begin{eqnarray}
\textbf{D} &=& \left[ \frac{I}{\Delta t} + [\textbf{B}_j] + [\textbf{B}_k] + [\textbf{B}_l]\right] \nonumber \\
\textbf{O}_{\zeta} &=& ([\textbf{A}_j],\ 0,\ [\textbf{C}_j]) \nonumber \\ 
\textbf{O}_{\eta} &=& ([\textbf{A}_k],\ 0,\ [\textbf{C}_k]) \nonumber \\ 
\textbf{O}_{\xi} &=& ([\textbf{A}_l],\ 0,\ [\textbf{C}_l])
\label{eqn:DDADI2}
\end{eqnarray}

For the DDLGS scheme a Gauss-Seidel \textcolor{Blue}{scheme} is performed on a line basis, i.e. implicit
inversion is performed for each line and the changes in the conservative
variables ($\Delta \textbf{q}$) are updated to the right-hand-side of
subsequent lines as soon as a new update is available, i.e.,
\begin{equation}
\begin{split}
[\textbf{A}_j]\Delta \textbf{q}_j +[\textbf{D}]\Delta \textbf{q}_{\rm jkl} &+[\textbf{C}_j]\Delta \textbf{q}_{\rm j+1} =  -\textbf{R}(\textbf{q}^n) \\
& -[\textbf{A}_k] \Delta \textbf{q}_{\rm k-1}^* -[\textbf{C}_k]\Delta \textbf{q}_{\rm k+1}^* \\
& -[\textbf{A}_l] \Delta \textbf{q}_{\rm l-1}^* -[\textbf{C}_l]\Delta \textbf{q}_{\rm l+1}^* 
\label{eqn:DDLGS}
\end{split}
\end{equation}
\noindent with the diagonal matrix $\textbf{D}$ given by Eq.~\ref{eqn:DDADI2}. The values with the asterisk (*) indicate intermediate values obtained during the Gauss-Seidel sweep. To eliminate the sweep bias, symmetry is obtained by sweeping once in a prescribed direction through the Hamiltonian loops and then sweeping again in the reverse direction through the Hamiltonian loops. All of the above approximate factorization operators introduce factorization errors which at large values of time step can overwhelm the discretization errors and considerably hamper convergence. A detailed discussion on the applicability of each of these methods is described by Buelow~\cite{Buelow01} in the context of structured grids. In the context of the present paper, since the approximate factorization 
scheme closely mirrors the discretization scheme, the solution update obtained
by the approximate inversion results in fast non-linear convergence.

% ===================================================================
\subsection{Domain Decomposition}
% ===================================================================

Typical grid-based CFD meshes contain millions of volume cells making it impractical for the solution to be executed on a single processor. The developed Hamiltonian solver (both two-dimensional and three-dimensional) is parallelized such that it can be executed on distributed compute memory systems. The communication between multiple subdomains is performed using Message Passing Interface (MPI). Therefore, the overall grid is split into multiple subdomains and the flow solution within each subdomain is computed in parallel.

METIS~\cite{Karypis99}, an open-source program used for graph partitioning, is employed in the present work. A cell-to-cell connectivity graph (i.e., a graph that indicates which cells in the unstructured grid share a common edge), is constructed and METIS is used to divide the graph such that each partition (subdomain) has approximately an equal number of cells. The partitioning of the graph in such a manner ensures proper load balancing between the multiple processors when the solution is executed in parallel. The partitioned data is also used to construct the neighbour cell information across the subdomain boundaries to facilitate exchange of information between the subdomains. 

\begin{figure*}
\centering
\includegraphics[width=0.65\textwidth]{../Figures/AirfoilSubdivision.pdf}
%\vspace{-0.3in}
\caption{Domain decomposition of the NACA 0012 airfoil grid and associated Hamiltonian loops.}
\label{fig:AirfoilSubdivision}
\end{figure*}

The decomposition of the overall grid system can be performed primarily in two ways; divide the three-dimensional volume grid or divide the two-dimensional surface mesh and subsequently create the strands within each subdomain. Figure~\ref{fig:RepresentativeSubdivision} shows an example of the surface and volume partitioning of a sphere using METIS, with each colour indicating a unique subdomain. Table~\ref{table:METISOutput} shows the breakdown of the number of cells and nodes within each subdomain when partitioned either using the surface grid or the volume grid. The overall volume domain contains 2,160 cells and 2,420 nodes. Note that the number of cells are almost evenly balanced between the subdomains using either subdivision technique. While both methods yielded acceptable subdomains, the decomposition of the volume grid can result in a strand getting divided between multiple subdomains. Furthermore, this division of a strand among different processors makes it disadvantageous to exploit the template nature of strands; see Fig.~\ref{fig:StrandSchematic}(b). However, either a division of the surface or volume domain will result in the Hamiltonian loops being broken. In 
structured grid solution methods, increasing the number of sub-domains results
in deterioration of convergence because of long implicit-lines being broken 
into smaller segments. In contrast, the Hamiltonian loops are inherently small
and are unlikely to be split more than a few times even in the case there are 
a large number of sub-domains. This quality is demonstrated later in this 
paper where good scalability with little loss of convergence can be noted.

\begin{figure}
\centering
\includegraphics[width=0.45\columnwidth]{../Figures/RepresentativeSubdivision.pdf}
%\vspace{-0.3in}
\caption{Representative surface and volume partitioning of a sphere using METIS.}
\label{fig:RepresentativeSubdivision}
\end{figure}

\begin{table}[t]
\begin{center}
{\renewcommand{\arraystretch}{1.25} %<- modify value to suit your needs
\begin{tabular}{|c||c|c||c|c|}
\hline
 & \multicolumn{2}{|c||}{Surface} & \multicolumn{2}{c|}{Volume} \\
\hline 
Subdomain & Cells & Nodes & Cells & Nodes \\\hline
1 & 360 & 530 & 360 & 526 \\
2 & 360 & 530 & 360 & 545 \\
3 & 360 & 540 & 360 & 541 \\
4 & 369 & 550 & 360 & 531 \\
5 & 360 & 540 & 361 & 530 \\
6 & 351 & 530 & 359 & 525 \\\hline
\end{tabular} 
} % for arraystretch
\end{center}
%\hspace{1cm}
\caption{Distribution of cells across different processors by either volume or surface subdivision.}
\label{table:METISOutput}
\end{table}

By way of formulation and construction, each triangle is subdivided into multiple quadrilaterals. Therefore, domain partitioning using METIS can be performed either at the triangular cell stage or the quadrilateral cell stage. As there is considerable more algorithmic work required in identifying the neighbouring cells in adjacent subdomains at the quadrilateral cell stage, partitioning is performed at the triangular cell stage. Figure~\ref{fig:AirfoilSubdivision} shows an example of domain decomposition on a NACA 0012 airfoil grid using the aforementioned methodology. The domain was divided into four subdomains and are indicated by the different colours. A zoomed in view of the airfoil shows the Hamiltonian loops formed on each subdomain. 

% ===================================================================
% END OF FILE
% ===================================================================
